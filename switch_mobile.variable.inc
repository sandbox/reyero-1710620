<?php
/**
 * @file
 * Switch mobile - Variable definition
 */

/**
 * Implements hook_variable_info().
 */
function switch_mobile_variable_info() {
  $variables['switch_mobile_url_standard'] = array(
    'title' => t('Site base URL'),
    'type' => 'url',
    'group' => 'switch_mobile',
  );
  $variables['switch_mobile_url_mobile'] = array(
    'title' => t('Mobile base URL'),
    'type' => 'url',
    'group' => 'switch_mobile',
  );
  $variables['switch_mobile_mobile_domain'] = array(
    'title' => t('Mobile domain'),
    'type' => 'path',
    'group' => 'switch_mobile',
  );
  $variables['switch_mobile_mobile_theme'] = array(
    'title' => t('Mobile theme'),
    'type' => 'string',
    'group' => 'switch_mobile',
  );
  return $variables;
}

/**
 * Implements hook_variable_group_info().
 */
function switch_mobile_variable_group_info() {
  $groups['switch_mobile'] = array(
    'title' => t('Switch mobile'),
  );
  return $groups;
}
