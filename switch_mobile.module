<?php
/**
 * @file
 * Switch mobile, Drupal module.
 *
 * Cookie value:
 * 1 = The site was switched, then switched back.
 * 1 = The site was switched, but not back.
 *
 */
define('SWITCH_MOBILE_AUTO_REDIRECT', 1);
define('SWITCH_MOBILE_MANUAL_REDIRECT', 2);

/**
 * Implements hook_boot()
 */
function switch_mobile_boot() {
  // Get current mode based on domain (standard|mobile)
  $domain_mode = switch_mobile_domain_mode();
  // Initialize the variable mobile realm before variable_mobile()
  module_invoke('variable_realm', 'initialize', 'mobile', $domain_mode);
  // If we are not in redirect mode, check cookies, domain and browser.
  if (!isset($_GET['switch_mobile'])) {
    if ($cookie = _switch_mobile_get_cookie()) {
      if ($cookie != $domain_mode) {
        _switch_mobile_boot_redirect($cookie);
      }
    }
    else {
      $browser_mode = switch_mobile_browser_mode();
      if ($domain_mode == $browser_mode) {
        // We are fine, just set the cookie.
        _switch_mobile_set_cookie($domain_mode);
      }
      else {
        // We need to redirect, set the cookie first if possible
        _switch_mobile_set_cookie($browser_mode);
        _switch_mobile_boot_redirect($browser_mode);
      }
    }
  }
}

/**
 * Redirect from bootstrap mode.
 */
function _switch_mobile_boot_redirect($mode) {
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
  if ($url = _switch_mobile_url($mode)) {
    drupal_goto($url);
  }
}

/**
 * Implements hook_init()
 */
function switch_mobile_init() {
  if (isset($_GET['switch_mobile'])) {
    // Set a cookie for switched clients so we don't switch again.
    _switch_mobile_set_cookie(switch_mobile_domain_mode());
  }
}

/**
 * Implements hook_custom_theme().
 */
function switch_mobile_custom_theme() {
  if (switch_mobile_check_domain() && $theme = variable_get('switch_mobile_mobile_theme')) {
    return $theme;
  }
}

/**
 * Get cookie value, FALSE if not set.
 */
function _switch_mobile_get_cookie() {
  $name = 'switch_mobile_' . switch_mobile_domain_mode();
  return isset($_COOKIE[$name]) ? $_COOKIE[$name] : FALSE;
}
/**
 * Set cookie value
 */
function _switch_mobile_set_cookie($value) {
  setrawcookie('switch_mobile_' . switch_mobile_domain_mode(), $value, REQUEST_TIME + 86400, '/');
}

/**
 * Check whether current mode (mobile, standard) matches the current browser.
 */
function _switch_mobile_browser_match() {
  return switch_mobile_check_browser() == switch_mobile_check_domain();
}

/**
 * Check whether it is a mobile browser.
 *
 * Bootstrap safe: Load browscap module if not loaded
 */
function switch_mobile_check_browser() {
  return variable_mobile_check_browser();
}

/**
 * Get mobile switch URL
 */
function _switch_mobile_url($mode, $manual = FALSE) {
  if ($base_url = variable_get('switch_mobile_url_' . $mode)) {
    $options['query']['switch_mobile'] = $manual ? 1 : 0;
    $options['base_url'] = $base_url;
    $options['absolute'] = TRUE;
    return url(current_path(), $options);
  }
}

/**
 * Get switching message.
 */
function _switch_mobile_message() {
  if (switch_mobile_check_domain()) {
    $message = t('This is the mobile version of the site. <a href="!switch-url">Switch to standard site</a>', array('!switch-url' => _switch_mobile_url('standard', TRUE)));
  }
  else {
    $message = t('This is the standard version of the site. <a href="!switch-url">Switch to mobile version</a>', array('!switch-url' => _switch_mobile_url('mobile', TRUE)));
  }
  return '<div class="switch-mobile-message">' . $message . '</div>';
}

/**
 * Implements hook_help()
 */
function switch_mobile_help($path, $args) {
  // Just display the help after an automatic switch.
  if (isset($_GET['switch_mobile']) && !$_GET['switch_mobile']) {
    return _switch_mobile_message();
  }
}

/**
 * Implements hook_block_info().
 */
function switch_mobile_block_info() {
  $blocks['switch']['info'] = t('Switch mobile');
  return $blocks;
}

/**
 * Implements hook_block_view()
 */
function switch_mobile_block_view($delta) {
  $block = array();
  switch ($delta) {
    case 'switch':
      $block['content'] = _switch_mobile_message();
      break;
  }
  return $block;
}

/**
 * Implements hook_field_attach_view_alter()
 *
 * Change build mode for some entities when on mobile mode. For this to work this module
 * needs a lower weight than other modules than build the display.
 */
function switch_mobile_field_attach_view_alter(&$build, &$context) {
  if (switch_mobile_check_domain() && isset($build['#entity_type']) && isset($build['#bundle']) && $mobile_mode = _switch_mobile_switch_display($build['#entity_type'], $build['#bundle'], $context['view_mode'])) {
    $context['view_mode'] = $mobile_mode;
    $build['#view_mode'] = $mobile_mode;
    foreach (element_children($build) as $key) {
      if (isset($build[$key]['#view_mode'])) {
        $build[$key]['#view_mode'] = $mobile_mode;
      }
    }
    // Build fields for the new view mode and replace existing ones.
    $fields = field_attach_view($build['#entity_type'], $context['entity'], $mobile_mode, $context['language']);
    $build = array_merge($build, $fields);
  }
}

/**
 * Check alternate mobile view modes for entity, view mode.
 *
 * The mobile view mode will be the same with a 'mobile_' prefix.
 */
function _switch_mobile_switch_display($entity_type, $bundle, $view_mode) {
  if ($mobile_mode = _switch_mobile_display_name($view_mode)) {
    $mode_settings = field_view_mode_settings($entity_type, $bundle);
    if (isset($mode_settings[$mobile_mode]) && !empty($mode_settings[$mobile_mode]['custom_settings'])) {
      return $mobile_mode;
    }
  }
  // Defaults to no mobile display.
  return FALSE;
}

/**
 * Get display mode name for switch mobile.
 */
function _switch_mobile_display_name($view_mode) {
  return strpos($view_mode, 'mobile_') === 0 ? NULL : 'mobile_' . $view_mode;
}

/**
 * Check whether the site is on the mobile domain.
 */
function switch_mobile_check_domain() {
  static $mobile;
  if (!isset($mobile)) {
    $domain = variable_get('switch_mobile_mobile_domain');
    $mobile = $domain && strpos($_SERVER['HTTP_HOST'], $domain) !== FALSE;
  }
  return $mobile;
}

/**
 * Get the current mode for the site depending on current domain (standard|mobile)
 */
function switch_mobile_domain_mode() {
  return switch_mobile_check_domain() ? 'mobile' : 'standard';
}

/**
 * Get the mode for the site checking browser domain (standard|mobile)
 */
function switch_mobile_browser_mode() {
  return switch_mobile_check_browser() ? 'mobile' : 'standard';
}

/**
 * Implements hook_ds_view_modes_info()
 */
function switch_mobile_ds_view_modes_info() {
  // Mobile teaser.
  $ds_view_mode = new stdClass;
  $ds_view_mode->disabled = FALSE;
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'mobile_teaser';
  $ds_view_mode->label = 'Mobile teaser';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $ds_view_modes[$ds_view_mode->view_mode] = $ds_view_mode;
  // Mobile full content.
  $ds_view_mode = new stdClass;
  $ds_view_mode->disabled = FALSE;
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'mobile_full';
  $ds_view_mode->label = 'Mobile full';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $ds_view_modes[$ds_view_mode->view_mode] = $ds_view_mode;
  return $ds_view_modes;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function switch_mobile_ctools_plugin_api($module, $api) {
  if (($module == 'ds' && $api == 'ds')) {
    return array('version' => 1);
  }
}
